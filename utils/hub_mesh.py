import paho.mqtt.client as mqtt
import json
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import requests
#import pysftp
import os
import espota
import send_ota
#Desabalitia plot do grafo na tela para não travar o software
plt.ioff()
os.chdir("/home/administrador/Documents/PlatformIO/Projects/fca-iot/utils/")

nodes =[]

##Path das imagens de grafos
#path_remoto = 'public_html/app/fc4/api'
#project_folder="/home/administrador/Documents/PlatformIO/Projects/fca-iot"
#path_imagens=project_folder+"/Imagens"




##Função para fazer upload de arquivos para o hostinger
#def uploadFtp(path_remoto,path_local): 
#   try:
#        print("Salvando na nuvem...")    
#        sftp = pysftp.Connection(server, username=username, password=password, port=port, cnopts=cnopts) 
#        sftp.cwd(path_remoto)
#        arquivos = os.listdir(path_local)                                             
#        for arquivo in arquivos:
#            x = path_local +'/'+ arquivo
#            print(x)
#            sftp.put(x)    
#        print("salvo..")
#        sftp.close()
#   except:
#         print("erro upload")





#Inscrever-se nos tópicos após a conexão mqtt
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("esp8266-out/#")
    client.subscribe("esp8266-in/#")
    client.subscribe("broker/#")


#Função que recebe as informações dos nodes e usa a biblioteca networkx para plotar os gráficos da topologia de rede
def plot_grafo(nodes): 
    label = 1  
    nodes=pd.DataFrame.from_dict(nodes)
    print(nodes)
    blue=(0,0,1)
    green=(0,1,0)
    G=nx.DiGraph()    
    #Varre os dados criando o grafo com cada node
    for origem in enumerate(nodes['top']):
        achou = 0
        for destino in enumerate(nodes['soft_mac']):
            if origem[1] == destino[1]:
                G.add_node(origem[label],color=blue)            
                G.add_node(destino[label],color=blue)
                G.add_edge(nodes['soft_mac'][origem[0]],destino[1])
                print(nodes['soft_mac'][origem[0]],destino[label])
                achou = 1
        if achou ==0:
            G.add_node(nodes['soft_mac'][origem[0]],color=blue)
            G.add_node('roteador',color=green)
            G.add_edge(nodes['soft_mac'][origem[0]],'roteador')    
    color=nx.get_node_attributes(G,'color')
    color=list(color.values())
    nx.draw_circular(G,with_labels = True,font_size=15,font_weight='bold',node_color=color,arrowstyle='->',arrowsize=22,width=2) 
    #Cria e redimensiona o grafo
    fig = plt.gcf()
    fig.set_size_inches(18.5, 10.5)
    print("Plotting...")
    #Salva primeiro no HD da maquina
    fig.savefig(path_imagens+"/Graph.png", format="PNG")
    #E então envia para o servidor
    uploadFtp(path_remoto,path_imagens)
    print("Done!")
    return

#Esta função é chamada sempre quando chega uma mensagem em um dos tópicos que este software se inscreveu
def on_message(client, userdata, msg):
    global payload
    global nodes
    print(msg.topic)
    print(msg.payload.decode("utf-8"))
    out_in_topic = msg.topic.split('/')[0]
    #Checa comandos que veem da mesh para fora
    if out_in_topic == "esp8266-out":
        #Endereço de quem enviou
        address = msg.topic.split('/')[1]
        #Comando enviado
        comando = msg.topic.split('/')[2]
        #Comando informando é topologia
        if comando == "arq":
           decoded = json.loads(msg.payload.decode("utf-8"))
           decoded['mac']=msg.topic.split('/')[1]
           nodes.append(decoded)
           print(decoded)
        #O comando status serve para informar os valores das medidas de cada sensor
        elif comando == "status":
           address = msg.topic.split('/')[1]
           payload = json.loads(msg.payload.decode("utf-8"))
           valor = payload['valor']
           #Pega o valor do sensor e enviar via http request para a api existente de IOT
           url = "http://quatroio.com.br/app/fc4/api/update.php?valor="+str(valor)+"&chipid="+str(address)
           send_request(url)          
    elif msg.topic == "esp8266-in/broadcast/topology":
            nodes=[]
            print("cleaning nodes")
            
    # Cada comando enviado para este sofware pode ser tratado aqui
    if msg.topic == "broker/plot":  
            #Comando de plotagem que gera o grafo, salva e o envia pra nuvem         
            plot_grafo(nodes)
    elif msg.topic == "broker/ota_mesh":  
            #Comando para passar firmwares por ota mqtt
            print("ota")
            payload = json.loads(msg.payload.decode("utf-8"))
            node_id = payload('node_id')
            file = payload('arquivo')            
            print(payload)
            args = {
            'bin':file,
            'id':node_id,
            'topic':'',
            'intopic':'',
            'outtopic':'',
            'broker':'localhost',
            'port':'',
            'user':'',
            'password':'',
            'ssl':'',
            'node':'',
            }
            
            send_ota.main(args)
    elif msg.topic == "broker/ota_http":  
            #Comando para passar o ota via http
            payload = json.loads(msg.payload.decode("utf-8"))
            print(payload)
            ip_node = payload('destino')
            file = payload('arquivo')
            args=['-i'+ip_node,'-d','-f'+file]
            espota.main(args)
                          
    return
    
#A função vai futuramente checar se há conexão com a internet e caso não haja, ela pode armazenar os valores localmente para enviar depois
#Não esta finalizada
def send_request(url):
    global online
    try:
        print("online")
        r = requests.get(url)
        print(r.text)
        online = True
        
    except ConnectionError:
        print("offline")
        #client.publish("esp8266-in/broadcast/heartbeat",60000)
        online=False
        f= open("/home/administrador/Documents/PlatformIO/Projects/IOT/requests.txt","w+")
        f.write(url)
        f.close()
        
        
        
#Configura MQTT       
client = mqtt.Client(client_id="SupervisorioMQTT")
client.on_connect = on_connect
client.on_message = on_message

        
#Tenta o tablet como broker se não der ele conecta em si mesmo 
try:
    client.connect("192.168.43.237", 1883, 60)
except:
    client.connect("localhost", 1883, 60)




client.loop_forever()


