<?php

/*=======================================================================
| API utilizada para abastecimento do gráfico.
| Autor = Alvaro Ramos- Luiz H.- Gabriel H.
| data = 21-08-2018
|========================================================================*/

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','UTF-8');
date_default_timezone_set('America/Sao_Paulo');

//================================================================CONEXÃO
require_once('conexao.php');

//================================================================função
function linear_regression( $x, $y ) {
 
    $n     = count($x);     // number of items in the array
    $x_sum = array_sum($x); // sum of all X values
    $y_sum = array_sum($y); // sum of all Y values
 
    $xx_sum = 0;
    $xy_sum = 0;
 
    for($i = 0; $i < $n; $i++) {
        $xy_sum += ( $x[$i]*$y[$i] );
        $xx_sum += ( $x[$i]*$x[$i] );
    }
 
    // Slope
    $slope = ( ( $n * $xy_sum ) - ( $x_sum * $y_sum ) ) / ( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) );
 
    // calculate intercept
    $intercept = ( $y_sum - ( $slope * $x_sum ) ) / $n;
 
    return array( 
        'slope'     => $slope,
        'intercept' => $intercept,
    );
}

//============================================================================================ Consulta JSON
$tabela= $_GET["tabela"];
$periodo= $_GET["periodo"];

if ($periodo == "DIARIO") {
$sql_select_periodo= ("SELECT id, MAX(valor) AS valor, DATE(datetime) AS X FROM $tabela GROUP BY X ORDER BY X DESC LIMIT 8");
}
else if ($periodo =="SEMANAL"){
$sql_select_periodo= ("SELECT id, MAX(valor) AS valor, WEEK(datetime) AS X FROM $tabela GROUP BY X ORDER BY X DESC LIMIT 8");
}
else if ($periodo =="MENSAL"){
$sql_select_periodo= ("SELECT id, MAX(valor) AS valor, MONTH(datetime) AS X FROM $tabela GROUP BY X ORDER BY X DESC LIMIT 8");
}
else if ($periodo =="ANUAL"){
$sql_select_periodo= ("SELECT id, MAX(valor) AS valor, YEAR(datetime) AS X FROM $tabela GROUP BY X ORDER BY X DESC LIMIT 8");
}


//echo($sql_select_periodo);
$result=mysqli_query($sql_select_periodo);
$valores = array();
$ids = array();

$indice = 0;
while($row = mysqli_fetch_assoc($result)){
	$indice = $indice +1;
	if ($periodo == "DIARIO") {
		$row['id'] =  date("d/m",strtotime($row['X'])); 
	}	else{
		$row['id'] =  $row['X']; 
	}
	if (((int)$row['valor'])>0){
		$ids[]=$indice;
		$valor[]=$row['valor'];	
	}
		
		
	
	$valores[] = $row;


	}

$valor = array_reverse($valor);	
$resultado = linear_regression($ids,$valor);	
//$valor = array_reverse($valor);

$valores = array_reverse($valores);
$last = end($valores);
/*
foreach (range(1, 4) as $number) {
	$atual = end($valores);
    $atual['X'] = $last['X']+$number;
	$atual['valor']=$last['valor']+$resultado['slope']*$number;
	
	array_push($valores, $atual);
}


*/
$retorno = array (
'valores' => $valores,
'result' => $last,	

);

echo(json_encode($retorno)); 	  


mysqli_close($dblink);

?>