<?php

/*=======================================================================
| API utilizada para abastecimento do gráfico.
| Autor = Alvaro Ramos- Luiz H.- Gabriel H.
| data = 21-08-2018
|========================================================================*/

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','UTF-8');
date_default_timezone_set('America/Sao_Paulo');

//================================================================CONEXÃO
require_once('conexao.php');

//================================================================função
function linear_regression( $x, $y ) {
 
    $n     = count($x);     // number of items in the array
    $x_sum = array_sum($x); // sum of all X values
    $y_sum = array_sum($y); // sum of all Y values
 
    $xx_sum = 0;
    $xy_sum = 0;
 
    for($i = 0; $i < $n; $i++) {
        $xy_sum += ( $x[$i]*$y[$i] );
        $xx_sum += ( $x[$i]*$x[$i] );
    }
 
    // Slope
    $slope = ( ( $n * $xy_sum ) - ( $x_sum * $y_sum ) ) / ( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) );
 
    // calculate intercept
    $intercept = ( $y_sum - ( $slope * $x_sum ) ) / $n;
 
    return array( 
        'slope'     => $slope,
        'intercept' => $intercept,
    );
}

//============================================================================================ Consulta JSON
$tabela= $_GET["tabela"];



$sql = ("SELECT * FROM `$tabela` ORDER BY `id` DESC LIMIT 10");
$result=mysqli_query($sql);
$valores = array();

while($row = mysqli_fetch_assoc($result)){		
	$row['id'] =  date("H:i:s",strtotime($row['datetime'])); 	
	$valores[] = $row;
	}
$valores = array_reverse($valores);
$last = end($valores);
$now = new DateTime();
$last = new DateTime($last['datetime']);





$sql = ("SELECT * FROM `enderecos` WHERE `local` LIKE '$tabela'");
$result=mysqli_query($sql);
$row = mysqli_fetch_assoc($result);
$timeout = (int)$row['timeout'];
$calculo = 'PT'.$timeout.'S';
$last->add(new DateInterval($calculo));
$online = 1;
if($now > $last){
	$online = 0;	
}

$now = $now->format('Y-m-d H:i:s');
$row['data_pre'] = date("d-m-y H:i:s",strtotime($row['data_pre']));
$row['data_alarme'] = date("d-m-y H:i:s",strtotime($row['data_alarme']));
$retorno = array (
'valores' => $valores,
'parametros'=>$row,
'now'=>$now,
'timeout'=>$timeout,
'online'=>$online,
);

echo(json_encode($retorno)); 	  


mysqli_close($dblink);

?>
