<?php

/*=======================================================================
| API utilizada para abastecimento do gráfico.
| Autor = Alvaro Ramos- Luiz H.- Gabriel H.
| data = 21-08-2018
|========================================================================*/

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','UTF-8');
date_default_timezone_set('America/Sao_Paulo');

//================================================================CONEXÃO
require_once('conexao.php');

//================================================================função
function linear_regression( $x, $y ) {
 
    $n     = count($x);     // number of items in the array
    $x_sum = array_sum($x); // sum of all X values
    $y_sum = array_sum($y); // sum of all Y values
 
    $xx_sum = 0;
    $xy_sum = 0;
 
    for($i = 0; $i < $n; $i++) {
        $xy_sum += ( $x[$i]*$y[$i] );
        $xx_sum += ( $x[$i]*$x[$i] );
    }
 
    // Slope
    $slope = ( ( $n * $xy_sum ) - ( $x_sum * $y_sum ) ) / ( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) );
 
    // calculate intercept
    $intercept = ( $y_sum - ( $slope * $x_sum ) ) / $n;
 
    return array( 
        'slope'     => $slope,
        'intercept' => $intercept,
    );
}

//============================================================================================ Consulta JSON
$tabela= $_GET["tabela"];


//$sql_select_30 = ("SELECT id, MAX(valor) AS valor, DATE(datetime) AS X FROM $tabela GROUP BY X ORDER BY X DESC");
$sql_select_30 = ("SELECT id, MAX(valor) AS valor, datetime AS X FROM $tabela GROUP BY DATE(X) ORDER BY X DESC");
$result=mysqli_query($sql_select_30);
$valores = array();


$indice = 0;
while($row = mysqli_fetch_assoc($result)){
	$indice = $indice +1;
	
	
	if (((int)$row['valor'])>10){
		$ids[]=$indice;
		$corrente[]=$row['valor'];	
	}
		$row['id'] =  date("d/m",strtotime($row['X'])); 
	$row['X'] =  date("H:i:s",strtotime($row['X'])); 
	if ($indice <31){
	$valores[] = $row;
	}
		

	}
$corrente = array_reverse($corrente);
$primeiro_dia_do_conjunto = end($valores);
$primeiro_dia_do_conjunto = new DateTime($primeiro_dia_do_conjunto['X']);
$sql_alarme = "SELECT * FROM `enderecos` WHERE `local` LIKE '$tabela'";
$result=mysqli_query($sql_alarme);
$row = mysqli_fetch_assoc($result);
$pre_alarme = (int)$row['prealarme'];
$alarme = (int)$row['alarme'];
$resultado = linear_regression($ids,$corrente);
$dia_pre = ($pre_alarme-(float)$resultado['intercept'])/(float)$resultado['slope'];
$dia_pre = (int)abs($dia_pre);
$dia_alarme = ($alarme-(float)$resultado['intercept'])/(float)$resultado['slope'];
$dia_alarme = (int)abs($dia_alarme);
$intervalo_calculo_pre = 'P'.$dia_pre.'D';
$intervalo_calculo_alarme = 'P'.$dia_alarme.'D';
$pre_dia = clone($primeiro_dia_do_conjunto);
$pre_dia->add(new DateInterval($intervalo_calculo_pre));
$alarme_dia = clone($primeiro_dia_do_conjunto);
$alarme_dia->add(new DateInterval($intervalo_calculo_alarme));








$valores = array_reverse($valores);
$retorno = array (
'valores' => $valores,	
'dias_pre' => $data_pre,
'dias_alarme' => $dias_alarme,
'hoje' => $hoje,
'pre_dia' =>$pre_dia->format('d/m/Y'),
'alarme_dia' =>$alarme_dia->format('d/m/Y'),

);

echo(json_encode($retorno)); 	  


mysqli_close($dblink);

?>
