<?php
/*=======================================================================
| ESCRIPT PADRAO PARA CADASTRAR USUARIO
| Autor = Alvaro Ramos
| data = 26-06-2018
|========================================================================*/
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','UTF-8');
date_default_timezone_set('America/Sao_Paulo');
//=======================================================================
$db = "mydb";         //NOME DO BANCO DE DADOS
$dbu = "pi";        //NOME DE USUARIO DO BANCO DE DADOS
$dbp = "fiat1234";                 //SENHA DO BANCO DE DADOS
$host = "localhost";//SERVIDOR DO BANCO DE DADOS

$dblink = mysqli_connect($host,$dbu,$dbp);// VARIAVEIS DE CONEXAO COM O BANCO DE DADOS
$seldb = mysqli_select_db($db);           // SELECIONA O BANCO DE DADOS




function sendSMS($telefone,$msg){
	
	$url_sms = 'https://www.paposms.com/webservice/1.0/send/';
	$fields = array(
        "user"=>'alvaroxramos@gmail.com',
        "pass"=>'10060506',
        "numbers"=>$telefone,
        "message"=>$msg,
        "return_format"=>"json"
		);		

	$postvars = http_build_query($fields);		
	$result = file_get_contents($url_sms."?".$postvars);
	$result_array = json_decode($result, true);		
	return $result_array;
}


function sendPush($msg){

    $content = array(   
        "en" =>$msg
        );

    $fields = array(
        'app_id' => "0c852c99-34c6-401d-ad6f-0c7036cafb8e",
        'included_segments' => array('All'),
        'data' => array("foo" => "bar"),
        'large_icon' =>"ic_launcher_round.png",
        'contents' => $content
    );

    $fields = json_encode($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                               'Authorization: Basic MjdkYTc4MTktZTZhYy00NmJlLWIyODMtOTNkYTE3YmIyNjNk'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    

    $response = curl_exec($ch);
    curl_close($ch);

    
	$return["allresponses"] = $response;
	$return = json_encode( $return);	
	return $return;
}
function AjaxGET($url){
	// Get cURL resource
$curl = curl_init();
// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $url,
    CURLOPT_USERAGENT => 'Codular Sample cURL Request'
));
// Send the request & save response to $resp
$resp = curl_exec($curl);
// Close request to clear up some resources
curl_close($curl);
	
	
}


function linear_regression( $x, $y ) {
 
    $n     = count($x);     // number of items in the array
    $x_sum = array_sum($x); // sum of all X values
    $y_sum = array_sum($y); // sum of all Y values
 
    $xx_sum = 0;
    $xy_sum = 0;
 
    for($i = 0; $i < $n; $i++) {
        $xy_sum += ( $x[$i]*$y[$i] );
        $xx_sum += ( $x[$i]*$x[$i] );
    }
 
    // Slope
    $slope = ( ( $n * $xy_sum ) - ( $x_sum * $y_sum ) ) / ( ( $n * $xx_sum ) - ( $x_sum * $x_sum ) );
 
    // calculate intercept
    $intercept = ( $y_sum - ( $slope * $x_sum ) ) / $n;
 
    return array( 
        'slope'     => $slope,
        'intercept' => $intercept,
    );
}

?>
