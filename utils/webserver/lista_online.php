<?php

/*=======================================================================
| API utilizada para abastecimento do gráfico.
| Autor = Alvaro Ramos- Luiz H.- Gabriel H.
| data = 21-08-2018
|========================================================================*/

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','UTF-8');
date_default_timezone_set('America/Sao_Paulo');

//================================================================CONEXÃO
require_once('conexao.php');



//============================================================================================ Consulta JSON

$sql = ("SELECT `chipid`,`local` FROM `enderecos` WHERE `online` = 1 ORDER BY `local` DESC");
$result=mysqli_query($sql);
$valores = array();


while($row = mysqli_fetch_assoc($result)){
	$valores[] = $row;
	}



$retorno = array (
'valores' => $valores
);

echo(json_encode($retorno)); 	  


mysqli_close($dblink);

?>