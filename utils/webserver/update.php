<?php
/*=======================================================================
| API utilizada para abastecimento do gráfico.
| Autor = Alvaro Ramos- Luiz H.- Gabriel H.
| data = 21-08-2018
|========================================================================*/
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','UTF-8');
date_default_timezone_set('America/Sao_Paulo');
//================================================================CONEXÃO
require_once('conexao.php');

function sendSMS($telefone,$msg){
	
	$url_sms = 'https://www.paposms.com/webservice/1.0/send/';
	$fields = array(
        "user"=>'alvaroxramos@gmail.com',
        "pass"=>'10060506',
        "numbers"=>$telefone,
        "message"=>$msg,
        "return_format"=>"json"
		);		

	$postvars = http_build_query($fields);		
	$result = file_get_contents($url_sms."?".$postvars);
	$result_array = json_decode($result, true);		
	return $result_array;
}


function sendPush($msg){

    $content = array(   
        "en" =>$msg
        );

    $fields = array(
        'app_id' => "0c852c99-34c6-401d-ad6f-0c7036cafb8e",
        'included_segments' => array('All'),
        'data' => array("foo" => "bar"),
        'large_icon' =>"ic_launcher_round.png",
        'contents' => $content
    );

    $fields = json_encode($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                               'Authorization: Basic MjdkYTc4MTktZTZhYy00NmJlLWIyODMtOTNkYTE3YmIyNjNk'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    

    $response = curl_exec($ch);
    curl_close($ch);

    
	$return["allresponses"] = $response;
	$return = json_encode( $return);	
	return $return;
}
function AjaxGET($url){
	// Get cURL resource
$curl = curl_init();
// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $url,
    CURLOPT_USERAGENT => 'Codular Sample cURL Request'
));
// Send the request & save response to $resp
$resp = curl_exec($curl);
// Close request to clear up some resources
curl_close($curl);
	
	
}

//============================================================================================ Consulta JSON
$valor= (float)$_GET["valor"];
$chipid= $_GET["chipid"];




$sql = "SELECT * FROM `enderecos` WHERE `chipid` LIKE '$chipid'";
$result=mysqli_query($sql);


$row = mysqli_fetch_assoc($result);
if ($row == null) {
	$sql2 = "INSERT INTO `enderecos` (`chipid`, `local`, `alarme`, `prealarme`,`grandeza`,`max`,`min`) VALUES ('$chipid', '0', '0', '0','0','0','0')";
	$cria=mysqli_query($sql2);	
	if($cria){	
		echo "Acordando para rede com chipid $chipid ";	 
	}
	else{	
		echo "Houve um erro ao atualizar: " . mysqli_error();

	}
}
else{
	
	$tabela = $row['local'];	
	
	$prealarme = $row['prealarme'];
	$alarme = $row['alarme'];
	/*
	$sms_pre_data = date("d",strtotime($row['data_pre']));
	$sms_alarme_data=date("d",strtotime($row['data_alarme']));
	$hoje = date("d",strtotime("now"));
	*/

	$now = strtotime('now');
	$now=date('Y-m-d H:i:s',$now);	
	$email = $row['email'];
	$telefone = $row['telefone'];
	$url = $row['url'];
	$local = $row['local'];	
	$grandeza = $row['grandeza'];
	$unidade = $row['unidade'];
	$titulo = $row['titulo'];
	$thres_alarme = $row['thres_alarme'];
	$thres_prealarme = $row['thres_prealarme'];
	
//========================================SMS E PUSH ALARME E PRE ALARME=====================
	if ($valor < $alarme && $thres_alarme == 1){
		$query_update_alarme = ("UPDATE `enderecos` SET `thres_alarme` = '0' WHERE `chipid` LIKE '$chipid'");
		$seta_alarme=mysqli_query($query_update_alarme);	
	}
	if ($valor >$alarme){
		AjaxGET($url);
	}
	if ($valor > $alarme  && $thres_alarme == 0){	
		
		
		
		$query_update_alarme = ("UPDATE `enderecos` SET `thres_alarme` = '1'  WHERE `chipid` LIKE '$chipid'");
		$seta_alarme=mysqli_query($query_update_alarme);
	    $mensagem = 'Atenção! A '.$grandeza.' do '.$titulo.' - Funilaria, medindo: '.$valor.$unidade.', ultrapassou o valor '. $alarme.$unidade.' (alarme), em: '.$now;			
		$response = sendSMS($telefone,$mensagem);
		echo($response);		
		$response = sendPush($mensagem);
		echo($response);
//======================================= ##### EMAIL ############## ==============================================================
//===================================================================================================
		 $datahora=date('d-m-Y - H:i:s');
		 //$to = "marcos.sousa@fcagroup.com";
			$to = "conexaodigitalfiat@gmail.com";
			 // $to = "alvaroxramos@gmail.com";
			  //=================================================
			   $subject = "Painel do Robo´ !";
				   $body = 'Atenção! A '.$grandeza.' do '.$titulo.' - Funilaria, medindo: '.$valor.$unidade.', ultrapassou o valor '. $prealarme.$unidade.' (pre alarme), em: '.$now;
				//================================================   
		 if (mail($to, $subject, $body, $datahora)) {
		   echo("<p>Email enviado com sucesso ao remetente!</p>");
		  } else {
		   echo("<p>Falha ao enviar o email</p>");

  }
 //====================================================================================================== 		
	}
	
	if ($valor < $prealarme  && $thres_prealarme == 1){
		$query_update_prealarme = ("UPDATE `enderecos` SET `data_pre` = '$now',`thres_prealarme` = '0' WHERE `chipid` LIKE '$chipid'");
		$seta_pre=mysqli_query($query_update_prealarme);
		}	
	if ($valor > $prealarme && $thres_prealarme == 0){	
		
		$query_update_pre = ("UPDATE `enderecos` SET `data_pre` = '$now',`thres_prealarme` = '1' WHERE `chipid` LIKE '$chipid'");
		$seta_pre=mysqli_query($query_update_pre);
		$mensagem = 'Atenção! A '.$grandeza.' do '.$titulo.' - Funilaria, medindo: '.$valor.$unidade.', ultrapassou o valor '. $prealarme.$unidade.' (pre alarme), em: '.$now;
		$response = sendSMS($telefone,$mensagem);
		echo($response);		
		$response = sendPush($mensagem);
		echo($response);
//======================================= ##### EMAIL ############## ==============================================================
//===================================================================================================
		$datahora=date('d-m-Y - H:i:s');
		 //$to = "marcos.sousa@fcagroup.com";
			$to = "conexaodigitalfiat@gmail.com";
			//  $to = "alvaroxramos@gmail.com";
			  //=================================================
			   $subject = "Painel do Robo´ !";
				   $body = 'Atenção! A '.$grandeza.' do '.$titulo.' - Funilaria, medindo: '.$valor.$unidade.', ultrapassou o valor '. $prealarme.$unidade.' (pre alarme), em: '.$now;
				//================================================   
		 if (mail($to, $subject, $body, $datahora)) {
		   echo("<p>Email enviado com sucesso ao remetente!</p>");
		  } else {
		   echo("<p>Falha ao enviar o email</p>");

  }
 //====================================================================================================== 		
	}
	
	
	//==========================================================================================================================================
	
	$sql2 = "INSERT INTO `$tabela` (`id`, `valor`, `datetime`) VALUES (NULL, '$valor', '$now')";
	
	$insert=mysqli_query($sql2);	
		if($insert){	
			echo "Registrado na tabela $tabela com o chipid:$chipid ";	 
		}
		else{	
			echo "Houve um erro ao atualizar: " . mysqli_error();
					$sqlX="CREATE TABLE `u822706850_iot`.`$tabela` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `valor` FLOAT NOT NULL , `datetime` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) , PRIMARY KEY (`id`)) ENGINE = InnoDB;
";
	$resultX=mysqli_query($sqlX);
	$row = mysqli_fetch_assoc($resultX);
		}

	}	

mysqli_close($dblink);

?>