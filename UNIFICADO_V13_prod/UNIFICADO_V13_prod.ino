///////////////////////////////////////////////////////////////////////////////////////////////////
//                                 www.quatroio.com.br
//     inserir dados ao banco de dados  servidor externo sql utilizando php metodo get
//     Cliente : Fiat SCA.
//     Programador Alvaro Ramos / Gabriel Henrique / VALDINEY GONÇALVES,,,,,,,,,,,
//     Criado: 02/04/2017
//     modificado: 12/05/2018
//     Resumo da mdificação:
//     status: FUNCIONANDO
//     Proxima atualização: Inserir servidores auxiliares.



//#### BIBLIOTECAS ##//
#include "DHT.h"
#include <Wire.h>
#include "max6675.h"
#include "EmonLib.h"
#include <SPI.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
//#include <WiFiClient.h>
#include <ESP8266WiFiMulti.h>
ESP8266WiFiMulti WiFiMulti;
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

//DHT 0
//Termopar 1
//Corrente 2
//Vibracaao 3
int sensor = 3;


////////////////////////////////////////////////////////////CONFIGURAÇÃO DOS SENSORES////////////////////////////////
#define thermoDO   2
#define thermoCS   4
#define thermoCLK  5
#define DHTPIN 4
#define DHTTYPE DHT11
//#define url_calib  "http://quatroio.com.br/app/fc4/api/calib.php?chipid="
#define url_valor  "http://abrbe1dt0092916:5000Tudo/_api/iot/calib.php?chipid="
//#define url_valor  "http://quatroio.com.br/app/fc4/api/update.php?valor="
#define url_valor  "http://abrbe1dt0092916:5000/_api/iot/update.php?valor="
#define GetChipid  "&chipid="
EnergyMonitor emon1;
int pino_sct = A0;
int amostragem = 10;
int primeiro = 0;
int varia_temp_umidade = 0;
String path;




////////////////////////////////////////////////////////////CONFIGURAÇÃO DA WIFI///////////////////////////////////
String ChipId;
const char* ssid     = "AndroidAP2";
const char* password = "digitaltabe";
const char* ssid2    = "FG-Prod04";
const char* password2 = "Fiasa!Prod04##2013";


////////////////////////////////////////////////////////////CONFIGURAÇÃO DO TIPO DE SENSOR E INTERVALO DE MEDIDA///////////////////
unsigned long previousTime = millis();
const unsigned long interval = 1000;
float valor = 0;
float valor_u = 0;

int ciclos = 1;
DHT dht(DHTPIN, DHTTYPE);
MAX6675 thermocouple(thermoCLK, thermoCS, thermoDO);
float anterior = 0;


void setup()
{


  ////////////////////////////////////////////////////////////SETUP DO SENSOR DE CORRENTE E DA CONEXÃO WIFI////////////////////////////////
  emon1.current(pino_sct, 15);
  Serial.begin(115200);
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);
  //WiFiMulti.addAP(ssid, password);
  WiFiMulti.addAP(ssid2, password2);
  //WiFiMulti.addAP("AndroidAP2", "digitaltabe");
  //delay(1000);
  int tentativas = 0;
  while (WiFiMulti.run() != WL_CONNECTED) {
    if (tentativas > 10) {
      Serial.println("Connection Failed! Rebooting...");
      ESP.restart();
    }
    Serial.println(tentativas);
    tentativas++;
    delay(500);

  }
  ChipId = WiFi.macAddress();
  delay(500);
  Serial.println(ChipId);
  Serial.println(WiFi.localIP());
  String path = url_calib +ChipId+"&ip="+WiFi.localIP().toString();
  Serial.println(path);
  delay(100);
  HTTPClient http;
  http.begin(path);
  int httpCode = http.GET();
  if (httpCode > 0) {
    Serial.printf("[HTTP] GET... code: %d\n", httpCode);

    // file found at server
    if (httpCode == HTTP_CODE_OK) {
      http.writeToStream(&Serial);
    }
  } else {
    Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
  }

  http.end();

  
  




  ////////////////////////////////////////////////////////////SETUP DO UPLOAD VIA WIFI////////////////////////////////
  char __Chip[sizeof(ChipId)];
  ChipId.toCharArray(__Chip, sizeof(__Chip));
  ArduinoOTA.setHostname(__Chip);
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
  delay(1000);
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}

void loop() {
  ////////////////////////////////////////////////////////////CHECA O UPLOAD PELO WIFI//////////////////////////////////
  ArduinoOTA.handle();
  ////////////////////////////////////////////////////////////REINICIA CASO PERCA A WIFI////////////////////////////////
  if (WiFiMulti.run() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(1000);
    ESP.restart();
  }


  ////////////////////////////////////////////////////////////CONFIGURA DE ACORDO COM O TIPO DE SENSOR//////////////////
  if (primeiro <= 10) {
    valor = 0;
    double Irms = emon1.calcIrms(1480);
    primeiro++;
  } else {
    if (sensor == 0) {
      valor = dht.readTemperature();
      valor_u = dht.readHumidity();
      if (isnan(valor) || valor == 0){
      return;
      }
     // valor += dht.readTemperature();
       //delay(1000);
      //ciclos++;
    
     
      
    } else if (sensor == 1) {
      valor += thermocouple.readCelsius();
      ciclos++;
    } else if (sensor == 2) {

      double Irms = emon1.calcIrms(1480);
      float temp = (float)Irms * 2.56;
      valor += temp;
      ciclos++;
    }
   else if (sensor == 3) {
    valor = analogRead(A0);
    /*
     double Irms = emon1.calcIrms(1480);
     float temp = (float)Irms*10;
     valor = temp;*/
       

  }
}


////////////////////////////////////////////////////////////MONTA STRING PARA HTTP GET///////////////////////////////

//String GetValor = "valor="; // modificado nome da variavel.
//String GetChipid = "&chipid=";




////////////////////////////////////////////////////////////FAZ O HTTP GET A CADA INTERVALO//////////////////////////
unsigned long diff = millis() - previousTime;
if (diff > interval && primeiro > 10) {
  valor = valor / ciclos;
  if (sensor==0){
    varia_temp_umidade = !varia_temp_umidade;
  }
  if (varia_temp_umidade == 1) {
  path = url_valor + (String) valor_u + GetChipid + ChipId+ "_u"; 
  } else{
  path = url_valor + (String) valor + GetChipid + ChipId;
  }
  ciclos = 1;
  Serial.println();
  Serial.print("Requesting URL: ");
  Serial.println(path);
  HTTPClient http;

  http.begin(path);

  int httpCode = http.GET();
  if (httpCode > 0) {
    Serial.printf("[HTTP] GET... code: %d\n", httpCode);

    // file found at server
    if (httpCode == HTTP_CODE_OK) {
      http.writeToStream(&Serial);
    }
  } else {
    Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
  }

  http.end();
  previousTime += diff;
  if (sensor != 0){
  valor = 0;
  }
 
}

 
}
