/*
 *  Copyright (C) 2016 PhracturedBlue
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  HLW8012 code Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>
 */


/* See credentials.h.examle for contents of credentials.h */
#include "credentials.h"
#include "capabilities.h"
#include <ESP8266WiFi.h>
#include <FS.h>
#include <ESP8266MQTTMesh.h>

#define DH            1
//#define CORRENTE      1
//#define BARRAMENTO    1
//#define VIBRACAO      1

#define      MEDIA      0
#define      MIN        1
#define      MAX        2

#ifdef DH
    #include "DHT.h"
    #include <Wire.h>
    #define DHTPIN 4
    #define DHTTYPE DHT11    
    #define SAMPLE_TYPE MEDIA
    #define      FIRMWARE_ID        0x4455

    DHT dht(DHTPIN, DHTTYPE);
    
#elif defined CORRENTE
    #include "EmonLib.h"
    #define SCTPIN A0 
    #define SAMPLE_TYPE MAX  
    #define      FIRMWARE_ID        0x4454
    EnergyMonitor emon1;     

#elif defined BARRAMENTO
    #include "max6675.h"
    #include <SPI.h>
    #define thermoDO   2
    #define thermoCS   4
    #define thermoCLK  5
    #define SAMPLE_TYPE MAX
    #define      FIRMWARE_ID        0x4453
    MAX6675 thermocouple(thermoCLK, thermoCS, thermoDO);
#elif defined VIBRACAO
    #define      FIRMWARE_ID        0x4452
    #define SAMPLE_TYPE MEDIA
#endif



#define      SAMPLES_MAX        1000
#define      FIRMWARE_VER       "7.0"
//const char*  networks[]       = NETWORK_LIST;
const wifi_conn networks[] = {
    WIFI_CONN("AndroidAP2", "digitaltabe", NULL, false),
    NULL
};
const char*  mesh_password    = MESH_PASSWORD;
const char*  mqtt_server      = MQTT_SERVER;
//const char*  base_ssid      = BASE_SSID;

ESP8266MQTTMesh mesh = ESP8266MQTTMesh::Builder(networks, mqtt_server)
                     .setVersion(FIRMWARE_VER, FIRMWARE_ID)
                     //.setMeshSSID(BASE_SSID)                     
                     .setMeshPassword(mesh_password)
                     .build();


int  heartbeat  = 1000;
float soma = 0.0;
float max_value = 0.0;
float min_value = 0.0;
float valor_anterior = 0.0;
float valor_enviar = 0.0;
float calib_min = 0.0;
float calib_max = 1023;
float valor_atual = 0; 
int sample_type = SAMPLE_TYPE;
int samples = 1;


void read_config();
void save_config();
void callback(const char *topic, const char *msg);
String build_json();

void setup() {
    #ifdef CORRENTE  
        emon1.current(SCTPIN, 15);
    #endif  
     
    Serial.begin(115200);
    SPIFFS.begin();
    delay(5000);
    mesh.setCallback(callback);
    mesh.begin();
    Serial.print("Version=");
    Serial.println(FIRMWARE_VER);
    //mesh.setup will initialize the filesystem
    if (SPIFFS.exists("/config")) {
        
        Serial.println("config file found...");
        read_config();
    }else{
        sample_type = MEDIA;
    }

   
    delay(1000);
    reset_measure();
    delay(1000);

}

void loop() { 
    
    static unsigned long lastSend = 0;
    unsigned long now = millis();
    
    #ifdef DH
        valor_atual = dht.readTemperature();   
    #elif defined CORRENTE
        double Irms = emon1.calcIrms(1480);
        valor_atual = (float)Irms * 2.56;      
    #elif defined BARRAMENTO
        valor_atual = thermocouple.readCelsius();     
    #elif defined VIBRACAO
        //valor_atual = 0;         
        valor_atual = analogRead(0);         
    #endif
        delay(10);
     
    if (!mesh.connected()) {   
        yield();        
        return;
    }
    if (sample_type == MEDIA){
        soma += valor_atual;
        samples += 1;
        valor_enviar = soma/samples;
    }else if (sample_type == MIN){
        if (valor_atual < min_value){
            min_value = valor_atual;
            valor_enviar = min_value;
        }
        
    }else if (sample_type == MAX){
        if (valor_atual > max_value){
            max_value = valor_atual;
            valor_enviar = max_value;
        }        
    }/*
    if (samples < SAMPLES_MAX){
        
    }*/
    
    if (now - lastSend > heartbeat) {
        String data = build_json();
        mesh.publish("status", data.c_str());
        lastSend = now; 
        reset_measure();  
    }
    
}
void reset_measure(){
    valor_enviar = 0;
    samples = 0;
    soma = 0;
    max_value = calib_min;
    min_value = calib_max;
}

void callback(const char *topic, const char *msg) {
    if (0 == strcmp(topic, "heartbeat")) {
       unsigned int hb = strtoul(msg, NULL, 10);
       if (hb > 1000) {
           heartbeat = hb;
           save_config();
       } 
    }
    else if (0 == strcmp(topic, "calib_max")) {
        calib_max = atof(msg);
        save_config();
    }
    else if (0 == strcmp(topic, "calib_min")) {
        calib_min = atof(msg);
        save_config();
    }
    else if (0 == strcmp(topic, "sample_type")) {
        sample_type = atoi(msg);
        save_config();
    }else if (0 == strcmp(topic, "topology")) {
        Serial.println("Topology");
        mesh.topology();
    }
}

String build_json() {
    String msg = "{";
    msg += "\"valor\":\"" + String(valor_enviar) + "\"";
    msg += "}";
    return msg;
}

void read_config() {
    File f = SPIFFS.open("/config", "r");
    if (! f) {
        Serial.println("Failed to read config");
        return;
    }
    while(f.available()) {
        Serial.println("Config READ");
        char s[32];
        char key[32];
        const char *value;
        s[f.readBytesUntil('\n', s, sizeof(s)-1)] = 0;
        if (! ESP8266MQTTMesh::keyValue(s, '=', key, sizeof(key), &value)) {
            continue;
        }        
        if (0 == strcmp(key, "heartbeat")) {
            heartbeat = atoi(value);
            if (heartbeat < 1000) {
                heartbeat = 1000;
            } else if (heartbeat > 60 * 60 * 1000) {
                heartbeat = 5 * 60 * 1000;
            }
        }else if (0 == strcmp(key, "calib_max")) {
            calib_max = atof(value);
        }else if (0 == strcmp(key, "calib_min")) {
            calib_min = atof(value);
        }else if (0 == strcmp(key, "sample_type")) {
            sample_type = atoi(value);
        }


    }
    f.close();
}

void save_config() {
    File f = SPIFFS.open("/config", "w");
    if (!f) {
        Serial.println("Failed to write config");
        return;
    }
    f.print("heartbeat=" + String(heartbeat) + "\n");
    f.print("calib_max=" + String(calib_max) + "\n");
    f.print("calib_min=" + String(calib_min) + "\n");
    f.print("sample_type=" + String(sample_type) + "\n");

    f.close();
}
